/* Order.ts */

export interface Order {
    orderId: number,
    orderNumber: string,
    orderKey: string,
    orderDate: string,
    createDate: string,
    modifyDate: string,
    paymentDate: string,
    shipByDate: string,
    orderStatus: string,
    customerId: number,
    customerUsername: string,
    customerEmail: string,
    /* billTo: { ... } */
    /* shipTo: { ... } */
    /*items: [{...}],*/
    orderTotal: number,
    amountPaid: number,
    taxAmount: number,
    shippingAmount: number,
    customerNotes: string,
    internalNotes: string,
    gift: boolean,
    giftMessage: string,
    paymentMethod: string,
    requestedShippingService: string,
    carrierCode: string,
    serviceCode: string,
    packageCode: string,
    confirmation: string,
    shipDate: string,
}