import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Order } from '../models/Order';
import { environment } from '../../environments/environment';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

	private apiUrl = environment.apiUrl;

	constructor(private http: Http) {
		// console.log(this.apiUrl);
	}

	public getOrders(options: any):Observable<any> {
		let URI = `${this.apiUrl}/get_orders`;
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let params = new URLSearchParams();
		params.append('customerName', options.customerName || '');
		params.append('pageSize', options.pageSize || 25);
		params.append('page', options.page || 1);
		return this.http.get(URI, {headers, params}).map(res => res.json())
	}
}
