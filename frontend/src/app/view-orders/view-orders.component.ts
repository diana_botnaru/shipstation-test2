import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../services/orders.service';
import { Order } from '../models/Order';

@Component({
	selector: 'app-view-orders',
	templateUrl: './view-orders.component.html',
	styleUrls: ['./view-orders.component.scss']
})
export class ViewOrdersComponent implements OnInit {

	private orders: Order[] = [];

	private dtOptions: DataTables.Settings = {};

	constructor(private ordersService: OrdersService) { }

	ngOnInit() {
		this.dtOptions = {
			pagingType: 'full_numbers',
			serverSide: true,
			processing: true,
			pageLength: 25,
  			ordering: false,
			ajax: (options: any, callback) => {
				this.ordersService.getOrders({
					customerName: options.search.value,
					pageSize: options.length,
					page: Math.round(options.start / options.length) + 1,
				}).subscribe(response => {
					this.orders = <Order[]>response.orders;
					callback({
						recordsTotal: response.total,
						recordsFiltered: response.total,
						data: []
					})
				});
			},
			columns: [{ data: 'orderNumber' }, { data: 'orderDate' }, { data: 'createDate' }, { data: 'orderTotal' }, { data: 'orderStatus' }]
		};
	}
}
