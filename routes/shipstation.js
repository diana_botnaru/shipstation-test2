var express = require('express');
var router = express.Router();
var ssAPIConfig = require('../config/shipstation');

/* shipstation ping */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

var shipstationAPI = require('node-shipstation');
var shipstation = new shipstationAPI(ssAPIConfig.apiKey, ssAPIConfig.apiSecret);

/* shipstation get orders */
router.get('/get_orders', (req, res, next) => {
	const options = {
		pageSize: req.query.pageSize || 25,
		page: req.query.page || 1,
		sortBy: 'OrderDate',
		sortDir: req.query.sortDir || 'DESC',
	};
	if (req.query.customerName) {
		options.customerName = req.query.customerName;
	}
	shipstation.getOrders(options, (err, response, body) => {
		if (err) {
			console.log(err);
			res.error(err);
		}
		res.send(body);
	});
})

module.exports = router;
